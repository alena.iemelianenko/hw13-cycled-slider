//1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval()
//setTimeout() це метод при якому функцію можна викликати один раз через певний проміжок часу. setInterval() - дозволяє викликати певну кількість разів через рівний промідок часу.

//2.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// функція спрацює миттево, але тільки після завершення виконання поточного коду. 

//3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
//setInterval() викликає функцію через певний проміжок часу. тому для того щоб його зупинити требе викликати функцію clearInterval().


//Завдання

let bntStop = document.querySelector('.stop');
let btnReStart = document.querySelector('.re-start');

let imgIndex = 0;
let timer

showImg();

function showImg() {

    let immageToShow = document.querySelectorAll('.image-to-show');

    for (let i = 0; i < immageToShow.length; i++ ) {
        immageToShow[i].style.display = "none";
    }

    imgIndex++;

    if (imgIndex > immageToShow.length) {imgIndex = 1}
    immageToShow[imgIndex-1].style.display = "block";
    timer = setTimeout (showImg, 3000);
};


bntStop.addEventListener('click', () => clearTimeout (timer));

btnReStart.addEventListener('click', () => showImg());
